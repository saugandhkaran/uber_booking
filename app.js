var bookingApp = angular.module("bookingApp",[]);

bookingApp.constant('given',{
    MAPS_API : "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial",
    UBER_API : "https://api.uber.com/v1.2/estimates/time",
    MAPS_PASS : "AIzaSyB6ky0s6kmaxH15hsxsNHKuZeI6n_OG2eA",
    UBER_PASS : "ECWcv5urK26d-pz-OHio9c9ovHpahx4UBbQIzMTi",
    MAIL_API : "https://api.sendgrid.com/api/mail.send.json?",
    DEVIATION_MAP : 60,
    MAX_DELAY_UBER : 15
});

bookingApp.factory('callsFactory',['$http','given', function($http, given){
    return {
            getMapTime : function(params){
                return $http.get(given.MAPS_API, {
                    params : params
                });
                },

            getCabArrivalTime : function(params){
                return $http.get(given.UBER_API,{
                    params : params,
                    headers: {
                            "Authorization" : 'Token ' + given.UBER_PASS
                                }
                            });
                },

            sendMail : function(params){
                return $http.post(given.MAIL_API + params,{
                    headers: {
                        'Authorization': "Bearer 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",
                        'Content-type': "application/json"
                    }
                    });
            }
    }
}]);

bookingApp.factory('utils',[function(){
    return {
        getLatLong : function(source){
            var sourceLatLong = {
                lat: source.split(",")[0],
                long: source.split(",")[1]
            }
            return sourceLatLong;
        },

        targetTime : function(scope){
            var timeOfDeparture = (scope.formObject.time.getHours())*60 + scope.formObject.time.getMinutes();
            return timeOfDeparture;
        },

        getUberGoTime : function(cabList){
            if(cabList.length > 0){
                for (var i = 0; i < cabList.length; i++ )
                {
                   if (cabList[i].display_name === "UberGO")
                    {
                        return Math.floor(cabList[i].estimate/60);
                    }
                }
            }
            else
                return 0;
        },

        getSafeTime : function(totalTime, targetTime){
            safeTime = targetTime - totalTime;
            return safeTime;
        },

        getCurrentTime : function(){
            var currentTime = new Date();
            return currentTime;
        }
    }

}]);

bookingApp.controller('bookingController',['$scope','given','callsFactory','utils','$interval','$timeout',function($scope, given, callsFactory, utils, $interval, $timeout){

    $scope.formObject = {};
    $scope.statusRecord = [];

    $scope.book = function() {
        getGoogleCall();
    }

    function getGoogleCall() { 
        callsFactory.getMapTime({
            origins : $scope.formObject.source,
            destinations : $scope.formObject.destination,
            mode : "driving",
            key : given.MAPS_PASS
        }).then( function(response){
            $scope.maxGoogleDeviation = Math.floor((response.data.rows[0].elements[0].duration.value)/60) + 60;
            $scope.statusRecord.push({"call" : "Google API", "time": utils.getCurrentTime().getHours().toString() + ':' + utils.getCurrentTime().getMinutes().toString()});
            getUberCall();
        }, function(error){
            alert(error);
        }) 
    }

    function getUberCall(){
        var startSource = utils.getLatLong($scope.formObject.source);
        callsFactory.getCabArrivalTime({
            start_latitude : startSource.lat,
            start_longitude : startSource.long
        }).then( function(response){
                $scope.statusRecord.push({"call" : "Uber API", "time": utils.getCurrentTime().getHours().toString() + ':' + utils.getCurrentTime().getMinutes().toString()} );
                $scope.uberMaxDeviation = Math.min(utils.getUberGoTime(response.data.times) , given.MAX_DELAY_UBER);
                var totalTime = $scope.uberMaxDeviation + $scope.maxGoogleDeviation;
                var targetTime = utils.targetTime($scope);
                var safeTime = utils.getSafeTime(totalTime, targetTime);
                var currentTime = utils.getCurrentTime().getHours() * 60 + utils.getCurrentTime().getMinutes();

                if (currentTime === safeTime){
                    $scope.statusRecord.push({"call" : "Sending Email ", "time": utils.getCurrentTime().getHours().toString() + ':' + utils.getCurrentTime().getMinutes().toString()} );
                        sendReminder();
                }
                else if (currentTime < safeTime){
                    if (!angular.isDefined($scope.safeTimer)) {
                        callingSafeZone((safeTime - currentTime)*60000);
                    }
                    
                }
                else{
                    if (!angular.isDefined($scope.worstTimer)) {
                        callingWorstZone(60000);
                    }
                }
        }, function(error){
            alert(error);
        })
    }

    function callingWorstZone(time){
        $scope.worstTimer = $interval(function(){
            getGoogleCall();
        }, time)
    }

    function callingSafeZone(time){
        $scope.safeTimer = $timeout(function(){
            getGoogleCall();
        },time);
    }

    function sendReminder(){
        var params =
        "api_user=" + "saugandhkaran" +
                        "&api_key=" + "saugandh123" +
                        "&to=" + $scope.formObject.email +
                        "&subject=" + "Reminder for your CAB" +
                        "&text=" + "Dear Customer, Please book the cab now!" +
                        "&from=" + "saugandhkaran@gmail.com"

        callsFactory.sendMail(params).then(function(response){
            $scope.statusRecord.push({"call" : "Email Sent", "time": utils.getCurrentTime().getHours().toString() + ':' + utils.getCurrentTime().getMinutes().toString() });
            console.log('sent');
        }, function(error){
            $scope.statusRecord.push({"call" : "Email Sent", "time": utils.getCurrentTime().getHours().toString() + ':' + utils.getCurrentTime().getMinutes().toString()});
            console.log('not sent')
        })
    }

}]);